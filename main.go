package main

import (
	"fmt"

	"github.com/schollz/progressbar/v3"
	"gitlab.com/mukund-kri/go-concurency-wine-dataset/config"
	"gitlab.com/mukund-kri/go-concurency-wine-dataset/loaders"
)

var dataFiles = []string{
	"data/winemag-data-130k-v2.csv",
	"data/winemag-data_first150k.csv",
}

func main() {

	// Compute vars needed for looping
	noFiles := len(dataFiles)

	config.InitializeDB()

	progress := make(chan int)

	for _, fileName := range dataFiles {
		go loaders.LoadToDB(progress, fileName, false)
	}

	// Add a progress bar to track the progress
	bar := progressbar.Default(int64(100 * noFiles))

	// Update progress bar
	for i := 0; i < 100*noFiles; i++ {
		<-progress
		bar.Add(1)
	}

	fmt.Println("Finished loading")

}
