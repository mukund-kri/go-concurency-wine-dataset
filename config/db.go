package config

import (
	"fmt"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

const (
	dbUser     = "assignment"
	dbPassword = "assignment"
	dbName     = "wine_reviews"
	dbPort     = "5432"
	dbHost     = "127.0.0.1"
)

// DB : the connection to postgres that is used through out the application
var DB *gorm.DB

// InitializeDB : initialize the connection to postgres
func InitializeDB() {

	dsn := fmt.Sprintf(
		"user=%s password=%s dbname=%s host=%s port=%s",
		dbUser,
		dbPassword,
		dbName,
		dbHost,
		dbPort,
	)

	var err error
	DB, err = gorm.Open(postgres.Open(dsn), &gorm.Config{})
	if err != nil {
		panic(err)
	}
}
