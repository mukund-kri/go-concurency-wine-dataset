module gitlab.com/mukund-kri/go-concurency-wine-dataset

go 1.15

require (
	github.com/schollz/progressbar/v3 v3.6.0
	gorm.io/driver/postgres v1.0.2
	gorm.io/gorm v1.20.2
)
