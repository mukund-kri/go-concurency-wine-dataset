package main

import (
	"gitlab.com/mukund-kri/go-concurency-wine-dataset/config"
	"gitlab.com/mukund-kri/go-concurency-wine-dataset/models"
)

func main() {

	config.InitializeDB()

	config.DB.AutoMigrate(&models.Review{})
}
