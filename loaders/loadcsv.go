package loaders

import (
	"encoding/csv"
	"os"
	"strconv"

	"gitlab.com/mukund-kri/go-concurency-wine-dataset/config"
	"gitlab.com/mukund-kri/go-concurency-wine-dataset/models"
)

// LoadToDB loads the contents of the csv file into db
func LoadToDB(progress chan int, csvFileName string) {

	// Open the csv file for reading
	csvFile, err := os.Open(csvFileName)
	if err != nil {
		panic("error opening file")
	}
	defer csvFile.Close()

	// Load the whole csv data into memory
	csvData, err := csv.NewReader(csvFile).ReadAll()
	if err != nil {
		panic(err)
	}

	// Start the staggered data load
	tx := config.DB.Begin()
	cycle := len(csvData) / 100

	for idx, record := range csvData {

		points, _ := strconv.Atoi(record[4])
		price, _ := strconv.ParseFloat(record[5], 64)

		review := models.Review{
			Country:     record[1],
			Description: record[2],
			Designation: record[3],
			Points:      uint(points),
			Price:       float32(price),
			Province:    record[6],
			Region1:     record[7],
			Region2:     record[8],
			Variety:     record[9],
			Winery:      record[10],
		}
		tx.Create(&review)

		if idx%cycle == 0 {
			tx.Commit()
			tx = config.DB.Begin()
			progress <- idx
		}
	}

}
