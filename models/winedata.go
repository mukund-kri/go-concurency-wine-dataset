package models

import (
	"gorm.io/gorm"
)

// Review : hold a single wine review from wine magazine
type Review struct {
	gorm.Model
	Country     string
	Description string
	Designation string
	Points      uint
	Price       float32
	Province    string
	Region1     string
	Region2     string
	Variety     string
	Winery      string
}
